package pcd.ass02.ex2;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

/**
 * Simple view of a Mandelbrot Set Image
 * 
 * @author aricci
 *
 */
public class MandelbrotView extends JFrame implements MouseMotionListener {

	private MandelbrotSetImage set;
	private MandelbrotPanel canvas;
	private JScrollPane scrollPane;
	
    private JButton start = new JButton("Start");
    private JButton stop = new JButton("Stop");
	private JPanel  commandsPanel = new JPanel();
	
	/* text fields reporting the position on the complex plain, given the mouse pointer */
	private JTextField posRe, posIm;
	
	private Thread repainter;
    private volatile boolean running = true;
	
	public MandelbrotView(MandelbrotSetImage set, int w, int h, double startRadius, int nIterMax) {
		super("Mandelbrot Viewer");
		setSize(w, h);
		this.setResizable(false);
		
		this.set = set;
		canvas = new MandelbrotPanel(set);
		canvas.setPreferredSize(new Dimension(set.getWidth(),set.getHeight()));
	    scrollPane = new JScrollPane(canvas);
	    
	    stop.setEnabled(false);

        start.addActionListener(e -> {
        	
            running = true;
            start.setEnabled(false);
            stop.setEnabled(true);

            repainter = new Thread(() ->{
                StopWatch cron = new StopWatch();
                double radius = startRadius;
                double time = 0.0;
                int iteration = 0;

                // Always true if not benchmarking, else it depends on the number of cycles
                
                while (running && radius >= Double.MIN_VALUE){
                    cron.start();
                    set.compute(nIterMax);
                    cron.stop();

                    time = cron.getTime();
                    iteration++;
                    
                    System.out.println("Radius: " + radius);
                    System.out.println("Frame " + iteration + " in " + time + " ms");

                    radius *= 0.98;
                    set.updateRadius(radius);
                    repaint();

                }


            });

            repainter.start();
        });

        stop.addActionListener(e ->{
            running = false;
            stop.setEnabled(false);
            start.setEnabled(true);
        });
	    
	    commandsPanel.add(start);
	    commandsPanel.add(stop);

	    JPanel info = new JPanel();
	    posRe = new JTextField(15);
	    posIm = new JTextField(15);
	    posRe.setEditable(false);
	    posIm.setEditable(false);
	    info.add(new JLabel("Re: "));
	    info.add(posRe);
	    info.add(new JLabel("Im: "));
	    info.add(posIm);
	    
	    setLayout(new BorderLayout());
	    add(scrollPane, BorderLayout.CENTER);		
		add(info,BorderLayout.NORTH);
		add(commandsPanel, BorderLayout.SOUTH);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		canvas.addMouseMotionListener(this);
	}

	/**
	 * When the mouse is moved, the position on the complex plane is updated  
	 */
	public void mouseMoved(MouseEvent e) {
		Complex point = set.getPoint(e.getX(),  e.getY());			
		posRe.setText(String.format("%.10f",point.re()));
		posIm.setText(String.format("%.10f",point.im()));
	}

	@Override
	public void mouseDragged(MouseEvent e) {}

		
	class MandelbrotPanel extends JPanel {

		private MandelbrotSetImage set;
		private BufferedImage image;
		
		public MandelbrotPanel(MandelbrotSetImage set) {
			this.set = set;
			image = new BufferedImage(set.getWidth(), set.getHeight(), BufferedImage.TYPE_INT_RGB);
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			image.setRGB(0, 0, set.getWidth(), set.getHeight(), set.getImage(), 0, set.getWidth());
			g2.drawImage(image, 0, 0, null);
		}

	}

}
