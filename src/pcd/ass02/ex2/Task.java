package pcd.ass02.ex2;

import java.util.concurrent.Callable;

public class Task implements Callable<Boolean>{
	
	private int w,h, y, nIterMax;
	private int image[];
	private double delta;
	private Complex center;
	
	public Task(int w, int h, Complex center, int y, int nIterMax, double radius, int[] image){
		this.w = w;
		this.h = h;
		this.y = y;
		this.center = center;
		this.nIterMax = nIterMax;
        this.image = image;
        delta = radius/(w*0.5);
	}
	
	public synchronized Boolean call(){	
		for (int x = 0; x < w; x++) {
			double x0 = (x - w*0.5)*delta + center.re();
			double y0 = center.im() - (y - h*0.5)*delta;
			double level = computeColor(x0,y0,nIterMax);
			int color = (int)(level*255);
			image[(y*w+x)] = color + (color << 8)+ (color << 16);
		}
		return true;
	}

	
	
	/**
	 * Basic Mandelbrot set algorithm optimized
	 *  
	 * @param c
	 * @param maxIteration
	 * @return
	 */
	private double computeColor(double x0, double y0, int maxIteration){
		int iteration = 0;		
		double x = 0;
		double y = 0;
		double x2 = x*x;
		double y2 = y*y;
		while (x2 + y2 < 4  &&  iteration < maxIteration) {
			double xtemp = x2 - y2 + x0;
			y = 2*x*y + y0;
			x = xtemp;
			x2 = x*x;
			y2 = y*y;
			iteration++;
		}
				  
		if ( iteration == maxIteration ){			  
			  /* the point belongs to the set */
			  return 0;
		  } else {
			  /* the point does not belong to the set => distance */
			  return 1.0-((double)iteration)/maxIteration;
		  }
	}

}
