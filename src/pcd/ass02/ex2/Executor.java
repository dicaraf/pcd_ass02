package pcd.ass02.ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Executor implements MandelbrotSetImage {
	
	private int image[];
	private int w, h;
    private Complex center;
    private double delta;

    public Executor(int w, int h, Complex center, double radius) {
    	this.w = w;
    	this.h = h;
        this.image = new int[w * h];
        this.center = center;
        this.delta = radius / (w * 0.5);
    }
    
    @Override
	public void compute(int nIterMax) {
    	ExecutorService       executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1);
        List<Future<Boolean>> futures         = new ArrayList<>();
        for (int y = 0; y < h; y++) {
            futures.add(executorService.submit(new Task(w, h, center, y, nIterMax, delta, image)));
        }

        for (Future<Boolean> future : futures) {
            try {
                future.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		
	}
	
	/**
	 * This method returns the point in the complex plane
	 * corresponding to the specified element/pixel in the image
	 * 
	 * @param x x coordinate in the image
	 * @param y y coordinate in the image
	 * @return the corresponding complex point
	 */
	public Complex getPoint(int x, int y){
		return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta); 
	}
	
	/**
	 * Get the height of the image
	 * @return
	 */
	public int getHeight(){
		return h;
	}

	/**
	 * Get the width of the image
	 * @return
	 */
	public int getWidth(){
		return w;
	}
	
	/**
	 * Get the image as an array of int, organized per rows
	 * (compatible with the BufferedImage style)
	 * 
	 * @return
	 */
	public int[] getImage(){
		return image;
	}
	
	
	public void updateRadius(double rad){
        delta = rad/(w*0.5);		
	}

	

}
