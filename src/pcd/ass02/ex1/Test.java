package pcd.ass02.ex1;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        int numThread = Runtime.getRuntime().availableProcessors();
        OracleInterface monitor = new Oracle(numThread);
        List<Player> playerList = new ArrayList<>();
        for(int i=1; i<=numThread; i++){
            playerList.add(new Player(i, monitor));
        }
        for(Player player : playerList){
            player.start();
        }
    }

}