package pcd.ass02.ex1;

public class ResultImpl implements Result {
    private boolean found;
    private boolean greater;
    private boolean less;

    public ResultImpl(boolean found, boolean greater, boolean less) {
        this.found = found;
        this.greater = greater;
        this.less = less;
    }

    @Override
    public boolean found() {
        return found;
    }

    @Override
    public boolean isGreater() {
        return greater;
    }

    @Override
    public boolean isLess() {
        return less;
    }
}
