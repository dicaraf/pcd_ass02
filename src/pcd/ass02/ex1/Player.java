package pcd.ass02.ex1;

import java.util.Random;

public class Player extends Thread {
    private Random random = new Random();
    private int playerNumber;
    private OracleInterface monitor;
    private long minSecretNumber = 0;
    private long maxSecretNumber = Long.MAX_VALUE;
    private Result result;

    public Player(int playerNumber, OracleInterface monitor) {
        this.playerNumber = playerNumber;
        this.monitor = monitor;
    }

    @Override
    public void run(){
        while(!monitor.isGameFinished()){
            try {
                final long guessNumber = minSecretNumber + ((long)(random.nextDouble()*(maxSecretNumber-minSecretNumber)));
                result = monitor.tryToGuess(playerNumber, guessNumber);
                if(result.isGreater()){
                    minSecretNumber = guessNumber;
                }else if(result.isLess()){
                    maxSecretNumber = guessNumber;
                }
            } catch (GameFinishedException e) {
            }
        }
        if(result.found()){
            System.out.println("[Player "+playerNumber+"] Won!");
        }else{
            System.out.println("[Player "+playerNumber+"] sob.");
        }
    }

}