package pcd.ass02.ex1;

import java.util.Random;
import java.util.concurrent.locks.Condition;

public class Oracle implements OracleInterface{
    private static final int RESTART_PLAYER = 0;
    private long secretNumber;
    private boolean gameFinished;
    private int currentPlayer;
    private int numPlayer;

    public Oracle(int numPlayer) {
        this.secretNumber = ((long)(new Random().nextDouble()*(Long.MAX_VALUE)));
        this.gameFinished = false;
        this.currentPlayer = RESTART_PLAYER;
        this.numPlayer = numPlayer;
    }

    @Override
    public synchronized boolean isGameFinished() {
        return gameFinished;
    }

    @Override
    public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {
        while(playerId != currentPlayer+1){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Player " + playerId + " try with " + value + ", numer is " + this.secretNumber);
        if (playerId == numPlayer)
            currentPlayer = RESTART_PLAYER;
        else
            currentPlayer = playerId;
        notifyAll();
        if (isGameFinished()) {
            throw new GameFinishedException();
        }
        if (value == secretNumber) {
            gameFinished = true;
        }
        return new ResultImpl(gameFinished, secretNumber > value, secretNumber < value);
    }
}